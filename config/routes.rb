Blog::Application.routes.draw do
  get "welcome/index"
  root 'posts#index'
  resources :articles do
    resources :comments
  end
end
